///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 03b - Animal Farm 1
///
/// @file animals.h
/// @version 1.0
///
/// Exports data about all animals
///
/// @author Destynee Fagaragan <djaf6@hawaii.edu>
/// @brief  Lab 03b - AnimalFarm1 - EE 205 - Spr 2021
/// @date   06.02.2021
///////////////////////////////////////////////////////////////////////////////
#include <stdbool.h>
#pragma once

/// Define the maximum number of cats or dogs in our array-database
#define MAX_SPECIES (20)

/// Gender is appropriate for all animals in this database
// enum Gender // @todo fill this out from here...
enum Gender { MALE, FEMALE };
enum catBreeds { MAIN_COON, MANX, SHORTHAIR, PERSIAN, SPHYNX };
enum Color { BLACK, WHITE, RED, BLUE, PINK, GREEN };

/// Return a string for the name of the color
/// @todo this is for you to implement
char* colorName (enum Color color);
char* breedName (enum catBreeds breed);
char* genderName (enum Gender gender);
char* isFixed (bool fixed);

