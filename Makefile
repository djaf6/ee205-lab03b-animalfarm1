###############################################################################
# University of Hawaii, College of Engineering
# EE 205  - Object Oriented Programming
# Lab 03b - Animal Farm 1
#
# @file Makefile
# @version 1.0
#
# @author Destynee Fagaragan <djaf6@hawaii.edu>
# @brief  Lab 03b - AnimalFarm1 - EE 205 - Spr 2021
# @date   06/02/2021
###############################################################################
all:animalfarm

animalfarm: main.o animals.o cat.o
	gcc -o animalfarm *.c

main.o: animals.h cat.h
cat.o: cat.h animals.h
animals.o: animals.h cat.h
clean:
	rm -f *.o animalfarm
