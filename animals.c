///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 03b - Animal Farm 1
///
/// @file animals.c
/// @version 1.0
///
/// Helper functions that apply to animals great and small
///
/// @author Destynee Fagaragan <djaf6@hawaii.edu>
/// @brief  Lab 03b - AnimalFarm1 - EE 205 - Spr 2021
/// @date   06.02.2021
///////////////////////////////////////////////////////////////////////////////

#include <stdlib.h>

#include "animals.h"

/// Decode the enum Color into strings for printf()
char* colorName (enum Color color) {

   // @todo Map the enum Color to a string
switch(color){
   case 0:
      return "Black";
   case 1:
      return "White";
   case 2:
      return "Red";
   case 3:
      return "Blue";
   case 4:
      return "Pink";
   case 5:
      return "Green";
   default:
       return NULL; // We should never get here
}
}
char* isFixed(bool fixed){
   switch (fixed){
      case true:
         return "Yes";
      case false:
         return "No";
   }

   return NULL;
}

char* breedName(enum catBreeds breed){
   switch(breed){
      case 0:
         return "Main Coon";
      case 1:
         return "Manx";
      case 2:
         return "Shorthair";
      case 3:
         return "Persian";
      case 4:
         return "Sphynx";
      default:
         return NULL;
   }
}

char* genderName(enum Gender gender){
   switch(gender){
      case 0:
         return "Male";
      case 1:
         return "Female";
      default:
         return NULL;
   }
}

